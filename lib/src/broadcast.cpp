#include "broadcast.h"

Broadcaster::Broadcaster(int value) : QObject(),  m_counter(value)
{
    connect(&timer, SIGNAL(timeout()), this, SLOT(tickHandler()));
    timer.start(1000);
}

void Broadcaster::tickHandler()
{
	QString msg = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss");
	emit broadcast(msg);
	if(m_counter > 0)
	{
		m_counter -= 2;
		if( m_counter <= 0)
			QCoreApplication::exit(m_counter);
	}
}
