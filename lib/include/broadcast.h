#ifndef brdcast_h__
#define brdcast_h__

#include <QtCore>

class Broadcaster : public  QObject
{
    Q_OBJECT
public:
    Broadcaster(int value);

signals:
    void broadcast(const QString msg);

private slots:
    void tickHandler();

private:
    QTimer timer;
    int m_counter;
};

#endif  // brdcast_h__
