macro(target_info tgt)

set(MSG COMMAND "${CMAKE_COMMAND}" -E echo )

add_custom_command(
TARGET ${tgt}
POST_BUILD
 ${MSG} ""
 ${MSG} "${tgt} target on $<PLATFORM_ID> config $<CONFIG>:"
 ${MSG} "$<TARGET_FILE:${tgt}>"
 ${MSG} ""
)

endmacro()
