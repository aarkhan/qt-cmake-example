#include "myapp.h"

MyApplication::MyApplication(int argc, char *argv[]) : QCoreApplication(argc, argv),
        out(stdout, QIODevice::WriteOnly)
{
    int val = 0;
    if(argc > 1)
        val = atoi(argv[1]);
    pb = new Broadcaster(val);
    if(!connect(pb, SIGNAL(broadcast(const QString)), this, SLOT(broadcastHandler(const QString))))
        qDebug("Error connecting signal");
}

void MyApplication::broadcastHandler(const QString msg)
{
    out << "\r" << msg;
    out.flush();
}
