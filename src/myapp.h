#include <QCoreApplication>
#include "broadcast.h"

class MyApplication : public QCoreApplication
{
    Q_OBJECT
public:
    MyApplication(int argc, char *argv[]);

public slots:
    void broadcastHandler(const QString msg);

private:
    Broadcaster * pb;
    QTextStream out;
};
