#include "myapp.h"

int main(int argc, char **argv)
{
  MyApplication app(argc, argv);
  qDebug("To exit press Ctrl-C...");
  return app.exec();
}
